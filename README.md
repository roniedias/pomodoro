** SERVER STARTUP **

1. Clone Repository;
2. Execute command: spring-boot:run

Obs.: Currently it is running with an in-memory database. It means that any data will exist only while server is up (any data will be lost on server startup)
 
=====================

** SWAGGER INFO **
http://localhost:8080/swagger-ui.html



=====================

** JWT INFO **

Issue a GET request to retrieve tasks with no JWT (HTTP 403 Forbidden status is expected)
curl http://localhost:8080/test

--

Register a new user
curl -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "password"}' http://localhost:8080/users/sign-up

--

Log into the application (JWT is generated)
curl -i -H "Content-Type: application/json" -X POST -d '{"username": "admin", "password": "password"}' http://localhost:8080/login

Token will be returned (in response Headers):
E.g.: Authorization: Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbiIsImV4cCI6MTY2NjQ2NzU0Mn0.Lapb3tvfarKecLfK9K0IH7pZ1nEF8DSP5zqacrW_ukn2C5CJWgX-8gMMpTHvAj45Hu0t88E6owLXa_7hyW1Hqw

--

Issue a GET request again to the test endpoint (passing the JWT this time). Remember to replace xxx.yyy.zzz with the JWT retrieved in the previous step
curl -H "Authorization: Bearer xxx.yyy.zzz" http://localhost:8080/test
